resource "kubernetes_ingress_v1" "sre-task" {
  wait_for_load_balancer = true
  metadata {
    name      = "sre-task-ingress"
    namespace = kubernetes_namespace.sre-task.id
    annotations = {
      "kubernetes.io/ingress.class" = "nginx"
    }
  }
  spec {
    rule {
      #   host = "backend.io"
      http {
        path {
          path      = "/"
          path_type = "Prefix"
          backend {
            service {
              name = kubernetes_service_v1.frontend.metadata.0.name
              port {
                number = 80
              }
            }
          }
        }
        path {
          path      = "/api/users/register/"
          path_type = "Prefix"
          backend {
            service {
              name = kubernetes_service_v1.backend.metadata.0.name
              port {
                number = 80
              }
            }
          }
        }
        path {
          path      = "/api/users/login/"
          path_type = "Prefix"
          backend {
            service {
              name = kubernetes_service_v1.backend.metadata.0.name
              port {
                number = 80
              }
            }
          }
        }
      }
    }
  }
}
