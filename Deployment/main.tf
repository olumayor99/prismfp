resource "kubernetes_secret" "database" {
  metadata {
    name = var.prefix
    #namespace = "sre-..."  # Insert your namespace here
    namespace = kubernetes_namespace.sre-task.id
  }

  data = {
    DATABASE_URI = var.uri_var_prefix
  }

  depends_on = [kubernetes_namespace.sre-task]
}