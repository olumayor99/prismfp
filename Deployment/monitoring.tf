resource "null_resource" "update_helm_repo" {
  provisioner "local-exec" {
    command = "helm repo update"
  }
}

resource "helm_release" "prometheus" {
  depends_on = [kubernetes_namespace.prometheus, null_resource.update_helm_repo]
  name       = "prometheus"
  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = "kube-prometheus-stack"
  namespace  = kubernetes_namespace.prometheus.id
  version    = "55.4.0"
  timeout    = 2000


  set {
    name  = "podSecurityPolicy.enabled"
    value = true
  }

  set {
    name  = "server.persistentVolume.enabled"
    value = false
  }

  set {
    name = "server\\.resources"
    value = yamlencode({
      limits = {
        cpu    = "200m"
        memory = "50Mi"
      }
      requests = {
        cpu    = "100m"
        memory = "30Mi"
      }
    })
  }

}

resource "helm_release" "metrics_server" {
  depends_on = [null_resource.update_helm_repo]
  name       = "metrics-server"
  namespace  = "kube-system"
  repository = "oci://registry-1.docker.io/bitnamicharts"
  chart      = "metrics-server"
  version    = "6.6.5"
}