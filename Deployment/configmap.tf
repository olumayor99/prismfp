resource "kubernetes_config_map" "sre-task" {
  metadata {
    name      = "sre-task"
    namespace = "sre-task"
  }

  data = {
    DATABASE_URI = var.uri_var_prefix
  }
}