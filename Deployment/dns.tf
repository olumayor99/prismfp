resource "aws_route53_zone" "ingress-nginx" {
  name = var.domain_name
}

data "aws_caller_identity" "current" {}

locals {
  oidc_issuer = var.oidc_issuer
}

data "aws_iam_policy_document" "external_dns_sts_policy" {
  statement {
    actions = ["sts:AssumeRoleWithWebIdentity"]
    effect  = "Allow"
    principals {
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:oidc-provider/${replace(local.oidc_issuer, "https://", "")}"]
      type        = "Federated"
    }
    condition {
      test     = "StringEquals"
      variable = "${replace(local.oidc_issuer, "https://", "")}:sub"
      values   = ["system:serviceaccount:default:external-dns"]
    }
  }
}

# IAM Role for ExternalDNS
resource "aws_iam_role" "external_dns" {
  assume_role_policy = data.aws_iam_policy_document.external_dns_sts_policy.json
  name               = "${var.prefix}-external-dns"
}

# IAM Policy for ExternalDNS
resource "aws_iam_policy" "external_dns" {
  name = "${var.prefix}-external-dns"
  policy = jsonencode({
    Statement = [{
      Action = [
        "route53:ListHostedZones",
        "route53:ListResourceRecordSets",
        "route53:ChangeResourceRecordSets"
      ]
      Effect   = "Allow"
      Resource = "*"
    }]
    Version = "2012-10-17"
  })
}

resource "aws_iam_role_policy_attachment" "external_dns_policy_attach" {
  role       = aws_iam_role.external_dns.name
  policy_arn = aws_iam_policy.external_dns.arn
}

resource "helm_release" "external-dns" {
  depends_on = [null_resource.update_helm_repo]
  name       = "external-dns"
  repository = "oci://registry-1.docker.io/bitnamicharts"
  chart      = "external-dns"
  version    = "6.28.6"

  set {
    name  = "provider"
    value = "aws"
    type  = "string"
  }

  set {
    name  = "aws.region"
    value = var.aws_region
    type  = "string"
  }

  set {
    name  = "txtOwnerId"
    value = aws_route53_zone.ingress-nginx.id
    type  = "string"
  }

  set {
    name  = "domainFilters[0]"
    value = var.domain_name
    type  = "string"
  }

  set {
    name  = "serviceAccount.name"
    value = "external-dns"
    type  = "string"
  }

  set {
    name  = "serviceAccount.create"
    value = "true"
    type  = "string"
  }

  set {
    name  = "serviceAccount.annotations.eks\\.amazonaws\\.com/role-arn"
    value = aws_iam_role.external_dns.arn
  }

  set {
    name  = "policy"
    value = "sync"
    type  = "string"
  }
}

resource "helm_release" "ingress-nginx" {
  name             = "ingress-nginx"
  repository       = "https://kubernetes.github.io/ingress-nginx"
  chart            = "ingress-nginx"
  namespace        = "ingress"
  version          = "4.8.4"
  create_namespace = true
  timeout          = 300

  set {
    name  = "cluster.enabled"
    value = "true"
  }

  set {
    name  = "metrics.enabled"
    value = "true"
  }

  set {
    name  = "rbac.create"
    value = "true"
  }
}

resource "local_file" "env_prod_url" {
  content  = templatefile("${path.module}/env_prod.tftpl", { env_prod_url = "${kubernetes_ingress_v1.sre-task.status.0.load_balancer.0.ingress.0.hostname}" })
  filename = "../app/frontend/src/environments/environment.prod.ts"
}