resource "kubernetes_service_v1" "frontend" {
  metadata {
    name      = "sre-frontend-service"
    namespace = kubernetes_namespace.sre-task.id
  }
  spec {
    selector = {
      app = kubernetes_deployment_v1.frontend.spec.0.template.0.metadata.0.labels.app
    }
    port {
      port        = 80
      target_port = 80
      protocol    = "TCP"
    }
    type = "ClusterIP"
  }
}

resource "kubernetes_service_v1" "backend" {
  metadata {
    name      = "sre-backend-service"
    namespace = kubernetes_namespace.sre-task.id
  }
  spec {
    selector = {
      app = kubernetes_deployment_v1.backend.spec.0.template.0.metadata.0.labels.app
    }
    port {
      port        = 80
      target_port = 8000
      protocol    = "TCP"
    }
    type = "ClusterIP"
  }
}