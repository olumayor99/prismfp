resource "kubernetes_namespace" "prometheus" {
  metadata {
    name = "prometheus"
  }
}

resource "kubernetes_namespace" "sre-task" {
  metadata {
    name = "sre-task"
  }
}