data "aws_eks_cluster" "cluster" {
  name = var.cluster_name
}

provider "aws" {
  region     = var.aws_region
  access_key = "AKIA5YZA3NUC6C62KPT3"
  secret_key = "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"

  default_tags {
    tags = {
      Project     = "SRExam"
      Owner       = "Karl"
      Environment = "neutron"
    }
  }
}


provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority[0].data)

  exec {
    api_version = "client.authentication.k8s.io/v1beta1"
    command     = "aws"
    args        = ["eks", "get-token", "--cluster-name", data.aws_eks_cluster.cluster.name]
  }
}

provider "helm" {
  kubernetes {
    host                   = data.aws_eks_cluster.cluster.endpoint
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority[0].data)

    exec {
      api_version = "client.authentication.k8s.io/v1beta1"
      command     = "aws"
      args        = ["eks", "get-token", "--cluster-name", data.aws_eks_cluster.cluster.name]
    }
  }
}