variable "prefix" {
  type        = string
  default     = "gredenskiy"
  description = "Prefix resource names"
}

variable "aws_region" {
  type        = string
  default     = "us-east-1"
  description = "Region"
}

variable "vpc_cidr" {
  type        = string
  default     = "10.10.0.0/16"
  description = "VPC CIDR range"
}

variable "uri_var_prefix" {
  type        = string
  description = "The URI of the RDS database"
  default     = "postgresql://demo:exyqvi1h@demodb-20231215085152155600000010.chvieoug9fcn.us-east-1.rds.amazonaws.com:5432/demo"
}

variable "uri_var_prefix2" {
  type        = string
  description = "The URI of the Test RDS database"
  default     = "postgresql://demo:exyqvi1h@demodb2-20231216035335597200000001.chvieoug9fcn.us-east-1.rds.amazonaws.com:5432/demo"
}

variable "domain_name" {
  type        = string
  default     = "drayco.com"
  description = "Domain name"
}

variable "ca_data" {
  type        = string
  default     = "LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSURCVENDQWUyZ0F3SUJBZ0lJVHhiSW5Wam1hNVl3RFFZSktvWklodmNOQVFFTEJRQXdGVEVUTUJFR0ExVUUKQXhNS2EzVmlaWEp1WlhSbGN6QWVGdzB5TXpFeU1UVXdPRFV4TlRCYUZ3MHpNekV5TVRJd09EVTJOVEJhTUJVeApFekFSQmdOVkJBTVRDbXQxWW1WeWJtVjBaWE13Z2dFaU1BMEdDU3FHU0liM0RRRUJBUVVBQTRJQkR3QXdnZ0VLCkFvSUJBUURnN0VSREw4NlJPVFZRQitIcDhRT1AwaHVpTW83U1NHcmFMRXhpNjA2c0FxK3ZUUFNDYi96bERwMG4KWElwVGZhTWl0am1mNWVJSysvOGFXZjVBMWRnWGFvTjVlK3BDdkRGcXQ1UHArYUFKUGxUOTZkb1NJVlpIdTFycgorSy9WcW56Uk00YXgrVFFGcEN3TXNxWnIzbDltMEt6Wmo3aitvSy8yMkd4a2E4Qm84VUpMKytSUi9aak91V29wCkVRUStiaTJWbmRFczBHV0daUU1HSUJTYTIyUjhZdUZWblRqTXdPT1ZPdFJ3MTVDWHR0QTkvWkJId3lMR1l2NGgKTFNLQ2VOWWZwczRHaVJXMm5SK3lzcWVXYlk2d2VXNXNZZDZuSkF2SDRGWm9DVW92bmpDdGRHci93UHJOQ1U5agpPenYyL0krU0dmZXg5dnViZ2lGTW9VTy90WXh6QWdNQkFBR2pXVEJYTUE0R0ExVWREd0VCL3dRRUF3SUNwREFQCkJnTlZIUk1CQWY4RUJUQURBUUgvTUIwR0ExVWREZ1FXQkJTbHVmRi9OLzdUcHc3Q3Q2Q3UyTlBrRHNMSlN6QVYKQmdOVkhSRUVEakFNZ2dwcmRXSmxjbTVsZEdWek1BMEdDU3FHU0liM0RRRUJDd1VBQTRJQkFRQlVsMnhDUVpwRQpBbW02clBwNVVOckd4ck5CQklpZEQyTzRLUEFYazVYeVpCb3NuS0NTaWhKTWlDRnd1U0tPSGJwWGJ5WXZWMWZFCkh2VzFXS2h0MytUT3FXdmhEc1ZiT2xQNS9YTWV0dkRKdnZRUDBQeG94L3BiQUIwbTlBQjlZU0dURnV5NlRLMTkKNTIwVENIQStoUys5bERZa0RiV3N4cjlzV2J5bzVBKzJORlJ6L1Vhb1BMMWZYeXJESnFrNGpQUDQzczMvM3V0cwpmQ095eS9nTDNVMWFSdy9UVjBQTmhwcFl5d1YxUmlqNlJpZ1ZZdkZhNnBXWlBjV0Z2V2R6VmQ2Rm52Y3loQTluClh5b29ubUdTVTBNUEp4RHhLNHFwRUlsdk1rMStkWkprY3lmSDRmV3V0c0ptQm54NXUycStzL1NpSG4xQ0VKMjQKbzdKUWJTRmdHdUU1Ci0tLS0tRU5EIENFUlRJRklDQVRFLS0tLS0K"
  description = "CA data of the EKS Cluster"
}

variable "cluster_endpoint" {
  type        = string
  default     = "https://5F43A6DD610A5542C68E94BB3EDE7427.gr7.us-east-1.eks.amazonaws.com"
  description = "Endpoint of the EKS Cluster"
}

variable "cluster_name" {
  type        = string
  default     = "gredenskiy-EKS"
  description = "Name of the EKS Cluster"
}

variable "oidc_issuer" {
  type        = string
  default     = "oidc.eks.us-east-1.amazonaws.com/id/5F43A6DD610A5542C68E94BB3EDE7427"
  description = "OIDC issuer of the EKS Cluster"
}

