resource "kubernetes_deployment_v1" "backend" {
  metadata {
    name      = "sre-backend"
    namespace = kubernetes_namespace.sre-task.id
  }

  spec {
    replicas = 2

    selector {
      match_labels = {
        app = "sre-backend"
      }
    }

    template {
      metadata {
        labels = {
          app = "sre-backend"
        }
      }

      spec {
        container {
          image = "olumayor99/sre-backend:latest"
          name  = "sre-backend"

          port {
            container_port = 8080
          }

          resources {
            limits = {
              cpu    = "0.5"
              memory = "512Mi"
            }
            requests = {
              cpu    = "250m"
              memory = "50Mi"
            }
          }

          #   liveness_probe {
          #     http_get {
          #       path = "/api"
          #       port = 8080
          #     }

          #     initial_delay_seconds = 3
          #     period_seconds        = 3
          #   }

          env_from {
            config_map_ref {
              name = "sre-task"
            }
          }
        }
      }
    }
  }
  depends_on = [kubernetes_config_map.sre-task]
}

resource "kubernetes_deployment_v1" "frontend" {
  metadata {
    name      = "sre-frontend"
    namespace = kubernetes_namespace.sre-task.id
  }

  spec {
    replicas = 2

    selector {
      match_labels = {
        app = "sre-frontend"
      }
    }

    template {
      metadata {
        labels = {
          app = "sre-frontend"
        }
      }

      spec {
        container {
          image = "olumayor99/sre-frontend:latest"
          name  = "sre-backend"

          port {
            container_port = 80
          }

          resources {
            limits = {
              cpu    = "0.5"
              memory = "512Mi"
            }
            requests = {
              cpu    = "250m"
              memory = "50Mi"
            }
          }

          liveness_probe {
            http_get {
              path = "/"
              port = 80
            }

            initial_delay_seconds = 3
            period_seconds        = 3
          }
        }
      }
    }
  }
}