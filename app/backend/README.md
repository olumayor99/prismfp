# Backend

Backend with REST API that allows to create and authorise users using JWT tokens


## Quick Start

1. From backend folder create virtual environment using python3 and activate it
```
$ virtualenv env
$ source env/bin/activate
```

2. Install dependencies in virtualenv
```
$ pip install -r requirements.txt
```

3. Setup flask
```
$ export FLASK_APP=run.py
```

4. Start test APIs server at `127.0.0.1:5000`
```
$ flask run
```

## Testing

Simply navigate using browser to `http://127.0.0.1:5000/` and using SwaggerUI try to make API calls.
