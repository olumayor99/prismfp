
resource "random_password" "sre-task-db-password" {
  length           = 8
  upper            = false
  numeric          = true
  special          = false
  override_special = "_%@"
}
resource "aws_secretsmanager_secret" "sre-task-db" {
  name = "${var.prefix}-sec"
}
resource "aws_secretsmanager_secret_version" "sre-task-db" {
  secret_id     = aws_secretsmanager_secret.sre-task-db.id
  secret_string = random_password.sre-task-db-password.result
}

resource "aws_db_subnet_group" "sre-task" {
  name       = "${var.prefix}-subnet-group"
  subnet_ids = module.vpc.public_subnets
}

module "security_group" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 5.0"

  name        = var.prefix
  description = "Complete PostgreSQL security group"
  vpc_id      = module.vpc.vpc_id

  ingress_with_cidr_blocks = [
    {
      from_port   = 5432
      to_port     = 5432
      protocol    = "tcp"
      description = "PostgreSQL access from within VPC"
      cidr_blocks = module.vpc.vpc_cidr_block
    },
  ]
}

module "db" {
  source = "terraform-aws-modules/rds/aws"

  identifier                     = "demodb"
  instance_use_identifier_prefix = true

  create_db_option_group    = false
  create_db_parameter_group = false

  engine               = "postgres"
  engine_version       = "14"
  family               = "postgres14"
  major_engine_version = "14"
  instance_class       = "db.t3.micro"

  allocated_storage = 5

  manage_master_user_password = false

  db_name  = "demo"
  username = "demo"
  password = random_password.sre-task-db-password.result
  port     = 5432

  db_subnet_group_name   = aws_db_subnet_group.sre-task.name
  vpc_security_group_ids = [module.security_group.security_group_id]

  maintenance_window      = "Mon:00:00-Mon:03:00"
  backup_window           = "03:00-06:00"
  backup_retention_period = 0
}

module "db2" {
  source = "terraform-aws-modules/rds/aws"

  identifier                     = "demodb2"
  instance_use_identifier_prefix = true

  create_db_option_group    = false
  create_db_parameter_group = false

  engine               = "postgres"
  engine_version       = "14"
  family               = "postgres14"
  major_engine_version = "14"
  instance_class       = "db.t3.micro"

  allocated_storage = 5

  manage_master_user_password = false

  db_name  = "demo"
  username = "demo"
  password = random_password.sre-task-db-password.result
  port     = 5432

  db_subnet_group_name   = aws_db_subnet_group.sre-task.name
  vpc_security_group_ids = [module.security_group.security_group_id]

  maintenance_window      = "Mon:00:00-Mon:03:00"
  backup_window           = "03:00-06:00"
  backup_retention_period = 0

  publicly_accessible = true
}

resource "local_file" "vars_file" {
  content  = templatefile("${path.module}/db_uri.tftpl", { database_uri = "postgresql://demo:${nonsensitive(random_password.sre-task-db-password.result)}@${module.db.db_instance_endpoint}/demo", database2_uri = "postgresql://demo:${nonsensitive(random_password.sre-task-db-password.result)}@${module.db2.db_instance_endpoint}/demo", prefix = "${var.prefix}", vpc_cidr = "${module.vpc.vpc_cidr_block}", aws_region = "${var.aws_region}", domain_name = "${var.domain_name}", cluster_endpoint = "${module.eks.cluster_endpoint}", cluster = "${module.eks.cluster_name}", ca_data = "${module.eks.cluster_certificate_authority_data}", oidc_issuer = "${module.eks.oidc_provider}" })
  filename = "../Deployment/variables.tf"
}