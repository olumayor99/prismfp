# Welcome
This project builds on top of a working EKS cluster with Prometheus, Loki and Grafana, cert-manager and the NGINX Ingress Controller.
Autoscaling has been limited to help display stress under heavy load.

# Steps to Start
1. Log in to [AWS Console](https://prismfp-test.signin.aws.amazon.com/console) with a username identical to **this** git repo, `sre-...` - credentials can be reset
2. Create an access key and ensure aws-cli is using it by default
3. Ensure the `kubeconfig_...` file is established as the default KUBECONFIG
4. Create a namespace with the same name as **this** git repo `sre-...`
5. `terraform init`
6. Create a new (and cost-effective) RDS instance by updating and applying the `rds.tf` file (update the namespace associated with the Kubernetes Secret that will be provisioned)

# Deploying main application
1. Containerise each application independently
3. Setup the Kubernetes deployment (pure k8s YAML or helm charts)
2. Deploy the apps in the namespace you created above
4. Launch app. Use the RDS instance created in the previous steps
5. Be sure that the production environment is stable enough and future maintainable

> Developers provided you with short instructions on how they usually run their code locally, but you are free not to follow them.

- [Frontend](app/frontend/README.md)
- [Backend](app/backend/README.md)

# For Discussion
You now have an EKS stack (plus a monitoring stack) with production workload on the cluster.
Describe how you would instrument and monitor the health of the application.

# Notes
- Traffic originating from nodes in the cluster is allowed into the default VPC (place RDS instances here!)
- Ingress resources should be deployed with domain entries ending in `.neutron.prismfp-dev.cloud`
- The [Grafana dashboard](https://grafana.neutron.prismfp-dev.cloud) can be accessed using the [kube-prometheus-stack defaults](https://stackoverflow.com/a/62793331/14638914)
